import { BasicClikPage } from './app.po';

describe('basic-clik App', function() {
  let page: BasicClikPage;

  beforeEach(() => {
    page = new BasicClikPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
