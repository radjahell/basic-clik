import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import {AboutComponent} from './about.component';
import {AppComponent} from './app.component';

// Route Configuration
export const routes: Routes = [  
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
    //If route is shit  
   {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);